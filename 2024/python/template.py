import fileinput

type InputType = list[str]
type SetupType = list[InputType]


def setup() -> SetupType:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    return [lines]


def part_one(lines: InputType) -> str:
    ...
    return "TODO"


def part_two(lines: InputType) -> str:
    ...
    return "TODO"


def main() -> None:
    input_data: SetupType = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
