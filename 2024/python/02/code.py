import fileinput
from functools import reduce
from itertools import pairwise

type Report = list[int]
type InputType = list[Report]
type SetupType = list[InputType]


def setup() -> SetupType:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [[int(val) for val in line.strip().split(" ")] for line in f]

    return [lines]


def is_safe(report: Report) -> bool:
    diffs = [a - b for (a, b) in pairwise(report)]
    all_positive = all([3 >= diff > 0 for diff in diffs])
    all_negative = all([-3 <= diff < 0 for diff in diffs])
    return all_positive or all_negative


def part_one(reports: list[Report]) -> int:
    return sum([1 for report in reports if is_safe(report)])


def is_safe_damped(report: Report) -> bool:
    for i in range(len(report)):
        damped = report[0:i] + report[i+1:]
        if is_safe(damped):
            return True
    return False


def part_two(reports: list[Report]) -> int:
    return sum([1 for report in reports if is_safe_damped(report)])


def main() -> None:
    input_data: SetupType = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
