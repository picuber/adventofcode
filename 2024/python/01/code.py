import fileinput

type InputType = tuple[list[int], list[int]]


def setup() -> InputType:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip().split("   ") for line in f]
        as_nums = [(int(line[0]), int(line[1])) for line in lines]
        left, right = map(list, zip(*as_nums))
    return left, right


def part_one(left: list[int], right: list[int]) -> int:
    return sum([abs(a - b) for a, b in zip(sorted(left), sorted(right))])


def part_two(left: list[int], right: list[int]) -> int:
    return sum([x * right.count(x) for x in left])


def main() -> None:
    input_data: InputType = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
