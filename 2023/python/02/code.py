import fileinput
from collections import defaultdict
from dataclasses import dataclass
from typing import TypedDict


@dataclass
class Draw(TypedDict):
    red: int
    green: int
    blue: int


@dataclass
class Game:
    id: int
    draws: list[Draw]


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    def parseDraw(draw_lst: list[str]) -> Draw:
        cubes: dict[str, int] = defaultdict(lambda: 0)
        for draw in draw_lst:
            d = draw.split(" ")
            cubes[d[1]] = int(d[0])
        return {"red": cubes["red"], "green": cubes["green"], "blue": cubes["blue"]}

    def parseGame(line: str) -> Game:
        game_str, draws_str = line.split(": ")
        draws_lst = [draw.split(", ") for draw in draws_str.split("; ")]
        return Game(
            int(game_str.split(" ")[1]), [parseDraw(draw) for draw in draws_lst]
        )

    return [parseGame(line) for line in lines]


def part_one(games: list[Game]):
    def possible(game: Game) -> bool:
        baseline = Draw(red=12, green=13, blue=14)
        for draw in game.draws:
            if (
                baseline["red"] < draw["red"]
                or baseline["green"] < draw["green"]
                or baseline["blue"] < draw["blue"]
            ):
                return False
        return True

    filtered = [game for game in games if possible(game)]
    return sum(map(lambda game: game.id, filtered))


def part_two(games: list[Game]):
    def power(game: Game) -> int:
        red, green, blue = 0, 0, 0
        for draw in game.draws:
            red = max(red, draw["red"])
            green = max(green, draw["green"])
            blue = max(blue, draw["blue"])
        return red * green * blue

    return sum(map(power, games))


def main() -> None:
    input_data: list = setup()
    print("Part 1:", part_one(input_data))
    print("Part 2:", part_two(input_data))


if __name__ == "__main__":
    main()
