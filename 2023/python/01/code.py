import fileinput
import re


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    return [lines]


def extract_digits(line: str, include_words: bool) -> int:
    word_int = {
        "0": 0,
        "1": 1,
        "2": 2,
        "3": 3,
        "4": 4,
        "5": 5,
        "6": 6,
        "7": 7,
        "8": 8,
        "9": 9,
        "zero": 0,
        "one": 1,
        "two": 2,
        "three": 3,
        "four": 4,
        "five": 5,
        "six": 6,
        "seven": 7,
        "eight": 8,
        "nine": 9,
    }

    regex = r"(?=(0|1|2|3|4|5|6|7|8|9"
    if include_words:
        regex += r"|zero|one|two|three|four|five|six|seven|eight|nine"
    regex += r"))"

    digits = [word_int[match] for match in re.findall(regex, line)]
    print(line, digits)
    return digits[0] * 10 + digits[-1]


def part_one(lines: list[str]):
    return sum([extract_digits(line, False) for line in lines])


def part_two(lines: list[str]):
    return sum([extract_digits(line, True) for line in lines])


def main() -> None:
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
