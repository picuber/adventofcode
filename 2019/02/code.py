with open('input.txt') as f:
    code_str = f.readline()
    code = list(map(int, code_str.split(',')))


def run(code, arg1, arg2):
    code = code[:]
    code[1] = arg1
    code[2] = arg2
    pc = 0

    while code[pc] != 99:
        if code[pc] == 1:
            code[code[pc + 3]] = code[code[pc + 1]] + code[code[pc + 2]]
        elif code[pc] == 2:
            code[code[pc + 3]] = code[code[pc + 1]] * code[code[pc + 2]]
        else:
            print('error')
            return
        pc += 4
    return code[0]


print(run(code, 12, 2))


for arg1 in range(100):
    for arg2 in range(100):
        if run(code, arg1, arg2) == 19690720:
            print(arg1, arg2, 100 * arg1 + arg2)
