with open('input.txt') as f:
    masses = [int(i) for i in f]

fuel = 0
for m in masses:
    fuel += m // 3 - 2

print(fuel)

fuel_rec = 0
for m in masses:
    step = m // 3 - 2
    while True:
        if step <= 0:
            break
        fuel_rec += step
        step = step // 3 - 2

print(fuel_rec)
