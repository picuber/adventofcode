with open('input.txt') as f:
    lines = f.readlines()

valid1 = 0
valid2 = 0
for line in lines:
    policy, pw = line.split(': ')
    minmax, letter = policy.split(' ')
    min_, max_ = map(int, minmax.split('-'))

    if min_ <= pw.count(letter) <= max_:
        valid1 += 1

    if bool(pw[min_ - 1] == letter) ^ bool(pw[max_ - 1] == letter):
        valid2 += 1

print(valid1)
print(valid2)
