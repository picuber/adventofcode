from typing import List, Tuple

import sys

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    instructions_str = [l.split(' = ') for l in f]


instructions: List[Tuple[str, str, List[str]]] = []
for ins, val_str in instructions_str:
    if ins == 'mask':
        instructions.append((ins, '', [c for c in val_str]))
    else:
        instructions.append(('mem', bin(int(ins[4:-1]))[2:].zfill(36), [c for c in bin(int(val_str))[2:].zfill(36)]))


mem = {}
mask = ['X'] * 36
for ins, idx, val in instructions:
    if ins == 'mask':
        mask = val
        continue
    val_masked = []
    for i in range(36):
        val_masked.append(val[i] if mask[i] == 'X' else mask[i])
    mem[idx] = int(''.join(val_masked), 2)

print(sum(mem.values()))


mem = {}
mask = ['0'] * 36
for ins, idx, val in instructions:
    if ins == 'mask':
        mask = val
        continue
    idx_masked = []
    for i in range(36):
        idx_masked.append(idx[i] if mask[i] == '0' else mask[i])

    idxs = [idx_masked]
    for i in range(36):
        if idx_masked[i] == 'X':
            idxs_ = idxs[:]
            idxs = []
            for idx_ in idxs_:
                idxs.append(idx_[:i] + ['0'] + idx_[i + 1:])
                idxs.append(idx_[:i] + ['1'] + idx_[i + 1:])

    for idx_ in idxs:
        mem[''.join(idx_)] = int(''.join(val), 2)
print(sum(mem.values()))
