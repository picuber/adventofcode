with open('input.txt') as f:
    groups = [g for g in f.read().strip().split('\n\n')]

anyone = [list(set([answer for answer in g.replace('\n', '')]))
          for g in groups]
total = 0
for a in anyone:
    total += len(a)

print(total)

everyone = [[[answer
              for answer in answers]
             for answers in group.split('\n')]
            for group in groups]
total = 0
for group in everyone:
    for answer in group[0]:
        if all(answer in person for person in group):
            total += 1
print(total)
