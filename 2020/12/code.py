import sys

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    instructions = [(i[0], int(i[1:])) for i in f]

cardinal = {'N': 0, 'E': 1, 'S': 2, 'W': 3}
rotation = {'R': 1, 'L': -1}


ns = 0
ew = 0
facing = 1


def move(amount, direction=None):
    global ns, ew
    if direction is None:
        direction = facing

    if direction == 0:
        ns += amount
    elif direction == 1:
        ew += amount
    elif direction == 2:
        ns -= amount
    elif direction == 3:
        ew -= amount


def turn(amount, direction):
    global facing
    amount //= 90
    facing = (facing + amount * direction) % 4


for i, v in instructions:
    if i in cardinal:
        move(v, cardinal[i])
    elif i == 'F':
        move(v)
    elif i in ['R', 'L']:
        turn(v, rotation[i])


print(abs(ns) + abs(ew))


ns = 0
ew = 0
wayns = 1
wayew = 10


def moveW(amount, direction):
    global wayns, wayew

    if direction == 0:
        wayns += amount
    elif direction == 1:
        wayew += amount
    elif direction == 2:
        wayns -= amount
    elif direction == 3:
        wayew -= amount


def turnW(amount, direction):
    global wayns, wayew
    wayns_ = wayns
    wayew_ = wayew
    if amount == 90:
        wayew = wayns_ * direction
        wayns = -wayew_ * direction
    elif amount == 180:
        wayns = -wayns
        wayew = -wayew
    elif amount == 270:
        wayew = -wayns_ * direction
        wayns = wayew_ * direction


def moveS(amount):
    global ns, ew
    ns += amount * wayns
    ew += amount * wayew


for i, v in instructions:
    if i in cardinal:
        moveW(v, cardinal[i])
    elif i == 'F':
        moveS(v)
    elif i in rotation:
        turnW(v, rotation[i])

print(abs(ns) + abs(ew))
