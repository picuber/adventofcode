import sys

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    numbers = list(map(int, f.readline().split(',')))

last = {}
step = 1
for n in numbers[:-1]:
    last[n] = step
    step += 1

n = numbers[-1]
step += 1
while True:
    if n in last:
        age = (step - 1) - last[n]
    else:
        age = 0
    last[n] = step - 1
    n = age

    if step == 2020:
        print(n)
    if step == 30000000:
        print(n)
        break

    step += 1
