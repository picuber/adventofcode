from typing import Dict, List, Tuple

import sys
import re


# direction <-> neighbour map
dn_map = {'north': 'up', 'east': 'right', 'south': 'down', 'west': 'left'}
nd_map = {'up': 'north', 'right': 'east', 'down': 'south', 'left': 'west'}
nnum_map = {'up': 0, 'right': 1, 'down': 2, 'left': 3}


class Tile:
    def __init__(self, tile_str):
        id_str, image_str = tile_str.split(':\n')
        self.id = int(id_str.split(' ')[1])
        self.image = [[c for c in l.strip()] for l in image_str.split('\n')]
        # neighbours
        self.n = {'up': None, 'right': None, 'down': None, 'left': None}

    def __str__(self):
        out = f'vv({self.id})vv\n'
        for line in self.image:
            out += ''.join(line) + '\n'
        out += f'^^({self.id})^^\n'
        return out

    def __repr__(self):
        return str(self.id)

    @property
    def edges(self):
        return {'north': ''.join(self.image[0]),
                'east': ''.join([self.image[i][-1] for i in range(len(self.image))]),
                'south': ''.join(self.image[-1])[::-1],
                'west': ''.join([self.image[i][0] for i in range(len(self.image))])[::-1],
                }

    @property
    def clean(self):
        return [row[1:-1] for row in self.image[1:-1]]

    def flip_ud(self):
        self.image = self.image[::-1]
        up = self.n['up']
        self.n['up'] = self.n['down']
        self.n['down'] = up

    def flip_lr(self):
        self.image = [row[::-1] for row in self.image]
        left = self.n['left']
        self.n['left'] = self.n['right']
        self.n['right'] = left

    def rotate(self, amount=1):
        """rotate clockwise by amount * 90 degree"""
        for _ in range(amount):
            rotated = [[self.image[i][j] for i in range(len(self.image[0]))][::-1] for j in range(len(self.image))]
            self.image = rotated

            up = self.n['up']
            self.n['up'] = self.n['left']
            self.n['left'] = self.n['down']
            self.n['down'] = self.n['right']
            self.n['right'] = up

    def get_n_dir(self, n_id):
        for direction, neighbour in self.n.items():
            if neighbour is None:
                continue
            if neighbour.id == n_id:
                return direction

    @staticmethod
    def get_rot_amnt(fix, flux):
        fix = nnum_map[fix]
        flux = nnum_map[flux]
        return (2 - (flux - fix) % 4)


infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    tiles_list = [Tile(t) for t in f.read().strip().split('\n\n')]
    tiles = {tile.id: tile for tile in tiles_list}

# find matches
matching: Dict[str, List[Tuple[Tile, str]]] = {}
for tile in tiles.values():
    for direction, edge in tile.edges.items():
        if edge in matching:
            matching[edge] += [(tile, direction)]
        elif edge[::-1] in matching:
            matching[edge[::-1]] += [(tile, direction)]
        else:
            matching[edge] = [(tile, direction)]

# add all tiles on the border of the image and only keep the ones with two edges on the border
corners = [match[0][0] for match in matching.values() if len(match) == 1]
for c in set(corners):
    corners.remove(c)

total = 1
for c in corners:
    total *= c.id
print(total)


# connect neighbours
for match in matching.values():
    if len(match) == 1:
        continue

    (tile1, d1), (tile2, d2) = match
    tile1.n[dn_map[d1]] = tile2
    tile2.n[dn_map[d2]] = tile1

# orient tiles
# start with one corner, put it at top-left, and work down line by lin
start = corners[0]
if start.n['up'] is not None:
    start.flip_ud()
if start.n['left'] is not None:
    start.flip_lr()

y_tiles_count = 1
line_start = start
while True:
    tile = line_start
    x_tiles_count = 1
    while True:
        right = tile.n['right']
        if right is None:
            break

        x_tiles_count += 1
        right.rotate(Tile.get_rot_amnt('right', right.get_n_dir(tile.id)))
        if tile.edges['east'] == right.edges['west']:
            right.flip_ud()

        tile = right

    down = line_start.n['down']
    if down is None:
        break
    y_tiles_count += 1
    down.rotate(Tile.get_rot_amnt('down', down.get_n_dir(line_start.id)))
    if line_start.edges['south'] == down.edges['north']:
        down.flip_lr()

    line_start = down

# line = start
# while line:
#     tile = line
#     while tile:
#         print(repr(tile), tile.n)
#         tile = tile.n['right']
#     print('---')
#     line = line.n['down']


# merge image
picture = []
line = start
while line:
    tile = line
    picture_line: List[List[str]] = [[] for _ in range(8)]
    while tile:
        clean = tile.clean
        for i in range(len(clean)):
            picture_line[i] += clean[i]
        tile = tile.n['right']
    picture.extend(picture_line)
    line = line.n['down']

# search sea monsters
sm_regex = ['..................#.',
            '#....##....##....###',
            '.#..#..#..#..#..#...']


def flip(pic):
    return pic[::-1]


def rotate(pic):
    return [[pic[i][j] for i in range(len(pic[0]))][::-1] for j in range(len(pic))]


ops = [rotate, rotate, rotate, rotate, flip, rotate, rotate, rotate, rotate]
matches = 0
while not matches:
    for row in range(len(picture[:-len(sm_regex)])):
        for col in range(len(picture[:-len(sm_regex)][row][:-len(sm_regex[0])])):
            sm_match = True
            for i in range(len(sm_regex)):
                if not re.match(sm_regex[i], ''.join(picture[row + i][col:])):
                    sm_match = False
                    break
            if sm_match:
                matches += 1
    break
    print(matches)
    picture = ops[0](picture)
    ops = ops[1:]
print(matches)
for l in picture:
    print(''.join(l))


for tile in tiles.values():
    print(repr(tile), tile.n)
