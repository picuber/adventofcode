from itertools import combinations

with open('input.txt') as f:
    values = [int(i) for i in f]

for a, b in combinations(values, 2):
    if a + b == 2020:
        print(a * b)

for a, b, c in combinations(values, 3):
    if a + b + c == 2020:
        print(a * b * c)
