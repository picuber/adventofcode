from typing import Dict, Any

import sys

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    rules_list, msgs = map(lambda x: x.strip().split('\n'), f.read().split('\n\n'))


rules: Dict[str, Any] = {k: list(map(lambda x: x.split(' '), v.split(' | ')))
                         for k, v in [r.split(': ') for r in rules_list]}
for nt in rules.keys():
    if rules[nt][0][0][0] == '"':
        rules[nt] = rules[nt][0][0][1]


def match(g, r, m):
    """ works for non-recursive rules, idk why not for recursive
    g: grammar / rules
    r: rule
    m: message
    """

    # print(m)
    if isinstance(g[r], str):
        if m and m[0] == g[r]:
            # print(f'match {r}:{g[r]} with {m[0]}')
            return True, m[1:]
        else:
            return False, m
    else:
        # print(f'>>>>> rule {r}')
        for alt in g[r]:
            # print(f'try alternative {alt}')
            m_ = m
            alt_matches = True
            for elem in alt:
                res, m_ = match(g, elem, m_)
                if not res:
                    alt_matches = False
                    break
            if alt_matches:
                # print('<<<<< rule {r}: MATCH')
                return True, m_
        # print('<<<<< rule {r}: FAIL')
        return False, m


matching = []
for msg in msgs:
    res, m_ = match(rules, '0', msg)
    if res and m_ == '':
        matching.append(msg)
print(len(matching))

rules['8'] = [['42'], ['42', '8']]
rules['11'] = [['42', '31'], ['42', '11', '31']]


# rest adapted from https://www.reddit.com/r/adventofcode/comments/kg1mro/2020_day_19_solutions/ggcnwa6/

def run_seq(g, seq, s):
    if not seq:
        yield s
    else:
        k, *seq = seq
        for s in run(g, k, s):
            yield from run_seq(g, seq, s)


def run_alt(g, alt, s):
    for seq in alt:
        yield from run_seq(g, seq, s)


def run(g, k, s):
    if isinstance(g[k], list):
        yield from run_alt(g, g[k], s)
    else:
        if s and s[0] == g[k]:
            yield s[1:]


def match2(g, s):
    return any(m == '' for m in run(g, '0', s))


matches = 0
for msg in msgs:
    if match2(rules, msg):
        matches += 1
print(matches)
