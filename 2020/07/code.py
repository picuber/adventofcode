with open('input.txt') as f:
    rule_strs = f.readlines()

rules = {}
for r in rule_strs:
    bag, content_str = r.split(' bags contain ')
    if 'no other' in content_str:
        content = {}
    else:
        content = {b[2:].strip(): int(b[0]) for b in
                   content_str[:-6]
                   .strip().replace('bags', 'bag').split('bag, ')}
    rules[bag] = content

outest = ['shiny gold']
outest_old = ['']
while outest != outest_old:
    outest_old = outest[:]
    for bag in outest_old:
        for b, c in rules.items():
            if bag in c:
                outest.append(b)
    outest = sorted(set(outest))
print(len(outest) - 1)


todo = ['shiny gold']
count = -1
while todo:
    todo_old = todo[:]
    todo = []
    for bag in todo_old:
        count += 1
        for b, n in rules[bag].items():
            todo += [b] * n
print(count)
