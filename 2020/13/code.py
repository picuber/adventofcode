import sys

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    time = int(f.readline())
    busses_str = f.readline().split(',')


def clean(busses_str_zip):
    busses = []
    for b, t in busses_str_zip:
        if b == 'x':
            continue
        busses.append((int(b), t))
    return busses


busses = clean(zip(busses_str, range(len(busses_str))))

waits = []
for bus, _ in busses:
    wait = bus - (time % bus)
    waits.append((wait, bus))
waits.sort()

print(waits[0][0] * waits[0][1])


def check(step, t):
    for i in range(step + 1):
        bus, pos = busses[i]
        if (t + pos) % bus != 0:
            return False
    return True


t = 0
delta = 1
step = 0
while True:
    t += delta
    if check(step, t):
        delta *= busses[step][0]
        step += 1
    if step >= len(busses):
        break
print(t)
