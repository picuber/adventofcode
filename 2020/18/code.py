import sys

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    expressions = [expr.strip().replace(' ', '') for expr in f.readlines()]


def calc(expr, idx=0):
    """
    Calculate until closing bracket or EOL
    An open bracket spawnes an reursive call
    Return the calculated value and the next index (after the closing bracket or EOL)
    """
    if expr[idx] == '(':
        value, idx = calc(expr, idx + 1)
    else:
        value = int(expr[idx])
        idx += 1

    while idx < len(expr):
        op = expr[idx]
        idx += 1
        if op == ')':
            return value, idx

        if expr[idx] == '(':
            val, idx = calc(expr, idx + 1)
        else:
            val = int(expr[idx])
            idx += 1

        if op == '+':
            value += val
        elif op == '*':
            value *= val

    return value, idx


total = 0
for expr in expressions:
    value, idx = calc(expr)
    total += value
print(total)


class Int(int):
    def __init__(self, value):
        self.value = value

    def __add__(self, other):
        return Int(self.value * other)

    def __mul__(self, other):
        return Int(self.value + other)


total = 0
for expr in expressions:
    ex = expr.replace('+', '#')
    ex = ex.replace('*', '+')
    ex = ex.replace('#', '*')
    for i in range(1, 10):
        ex = ex.replace(f'{i}', f'Int({i})')

    total += eval(ex)
print(total)
