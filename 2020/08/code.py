code = []
with open('input.txt') as f:
    for line in f:
        splt = line.split(' ')
        code.append((splt[0], int(splt[1]), False))


def run(code):
    code = code[:]
    pc = 0
    acc = 0
    while True:
        if pc >= len(code):
            break
        op, val, rep = code[pc]
        if rep:
            break
        code[pc] = (op, val, True)
        if op == 'acc':
            acc += val
            pc += 1
        elif op == 'jmp':
            pc += val
        elif op == 'nop':
            pc += 1
    return acc, pc, code


acc, pc, code_loop = run(code)
print(acc)

trace = [i for i in range(len(code)) if code_loop[i][2]]
for t in trace:
    code_ = code[:]
    if code_[t][0] == 'acc':
        continue
    elif code_[t][0] == 'jmp':
        op, val, rep = code_[t]
        code_[t] = ('nop', val, rep)
    elif code_[t][0] == 'nop':
        op, val, rep = code_[t]
        code_[t] = ('jmp', val, rep)
    acc, pc, _ = run(code_)
    if pc == len(code):
        break
print(acc)
