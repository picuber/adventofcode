with open('input.txt') as f:
    passports = [p.replace('\n', ' ').strip().split(' ')
                 for p in f.read().split('\n\n')]


valid_count = 0
valid_fields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
for passport in passports:
    fields = {f: v for f, v in map(lambda p: p.split(':'), passport)}

    if 'byr' not in fields \
            or len(fields['byr']) != 4 \
            or not (1920 <= int(fields['byr']) <= 2002):
        continue

    if 'iyr' not in fields \
            or len(fields['byr']) != 4 \
            or not (2010 <= int(fields['iyr']) <= 2020):
        continue

    if 'eyr' not in fields \
            or len(fields['byr']) != 4 \
            or not (2020 <= int(fields['eyr']) <= 2030):
        continue

    if 'hgt' in fields:
        hgt = fields['hgt']
        if 'cm' in hgt:
            if not (150 <= int(hgt[:-2]) <= 193):
                continue
        elif 'in' in hgt:
            if not (59 <= int(hgt[:-2]) <= 76):
                continue
        else:
            continue
    else:
        continue

    if 'hcl' in fields:
        hcl = fields['hcl']
        if len(hcl) == 7 and hcl[0] == '#':
            try:
                int(hcl[1:], 16)
            except ValueError:
                continue
        else:
            continue
    else:
        continue

    if 'ecl' in fields:
        ecl = fields['ecl']
        if ecl not in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']:
            continue
    else:
        continue

    if 'pid' in fields and len(fields['pid']) == 9:
        try:
            int(fields['pid'])
        except ValueError:
            continue
    else:
        continue

    valid_count += 1

print(valid_count)
