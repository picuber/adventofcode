def to_seat_id(seat):
    seat = seat.replace('F', '0')
    seat = seat.replace('B', '1')
    seat = seat.replace('L', '0')
    seat = seat.replace('R', '1')
    return int(seat, 2)


with open('input.txt') as f:
    seats = [to_seat_id(s) for s in f]

print(max(seats))

for s in range(1000):
    if s not in seats and (s - 1) in seats and (s + 1) in seats:
        print(s)
