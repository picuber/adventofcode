from typing import List

import sys

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    layout = [[c for c in l.strip()] for l in f]


def neighbors(a, r, c):
    result = []
    if r > 0 and c > 0:
        result += [a[r - 1][c - 1]]
    if r > 0:
        result += [a[r - 1][c]]
    if r > 0 and c < len(a[0]) - 1:
        result += [a[r - 1][c + 1]]
    if c > 0:
        result += [a[r][c - 1]]
    if c < len(a[0]) - 1:
        result += [a[r][c + 1]]
    if r < len(a) - 1 and c > 0:
        result += [a[r + 1][c - 1]]
    if r < len(a) - 1:
        result += [a[r + 1][c]]
    if r < len(a) - 1 and c < len(a[0]) - 1:
        result += [a[r + 1][c + 1]]
    return result


def update(layout, r, c):
    seat = layout[r][c]
    adj_occ = neighbors(layout, r, c).count('#')
    if seat == 'L' and adj_occ == 0:
        return '#'
    elif seat == '#' and adj_occ >= 4:
        return 'L'
    else:
        return seat


def step(layout):
    return [[update(layout, r, c) for c in range(len(layout[r]))]
            for r in range(len(layout))]


def print_layout(layout):
    print('-' * len(layout[0]))
    for l in layout:
        print(''.join(l))
    print('-' * len(layout[0]))


last_step: List[List[str]] = []
this_step = [r[:] for r in layout]
while last_step != this_step:
    last_step = this_step
    this_step = step(this_step)

print(sum([r.count('#') for r in this_step]))


def neighbors2(a, r, c):
    def look(dr, dc):
        r_ = r + dr
        c_ = c + dc
        while 0 <= r_ < len(a) and 0 <= c_ < len(a[0]):
            if a[r_][c_] == '#':
                return ['#']
            elif a[r_][c_] == 'L':
                return ['L']

            r_ += dr
            c_ += dc

        return []

    result = []
    result += look(-1, -1)
    result += look(-1, 0)
    result += look(-1, 1)
    result += look(0, -1)
    result += look(0, 1)
    result += look(1, -1)
    result += look(1, 0)
    result += look(1, 1)
    return result


def update2(layout, r, c):
    seat = layout[r][c]
    adj_occ = neighbors2(layout, r, c).count('#')
    if seat == 'L' and adj_occ == 0:
        return '#'
    elif seat == '#' and adj_occ >= 5:
        return 'L'
    else:
        return seat


def step2(layout):
    return [[update2(layout, r, c) for c in range(len(layout[r]))]
            for r in range(len(layout))]


this_step = [r[:] for r in layout]
while last_step != this_step:
    last_step = this_step
    this_step = step2(this_step)

print(sum([r.count('#') for r in this_step]))
