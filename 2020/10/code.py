import sys
from itertools import product

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    adapters = sorted([int(l) for l in f])

source = 0
sink = adapters[-1] + 3

diffs = []
for a, b in zip([source] + adapters, adapters + [sink]):
    diffs.append(b - a)
print(diffs.count(1) * diffs.count(3))

print(diffs)


def count_ones(diffs):
    counts = []
    count = 0
    for n in diffs:
        if n == 1:
            count += 1
        elif n == 3:
            if count == 0:
                continue
            counts.append(count)
            count = 0
    return counts


def count_to_opts(count):
    def valid(opts):
        count = 0
        for n in opts:
            if n == 0:
                count += 1
                if count == 3:
                    return False
            if n == 1:
                count = 0
        return True

    a = list(map(list, product(range(2), repeat=count - 1)))
    return [o for o in a if valid(o)]


res = 1
for n in count_ones(diffs):
    res *= len(count_to_opts(n))
print(res)
