import math

with open('input.txt') as f:
    trees = [[c == '#' for c in row.strip()] for row in f]

rows = len(trees)
cols = len(trees[0])


def check(right, down):
    row = 0
    col = 0
    count = 0
    while True:
        row += down
        col = (col + right) % cols
        if row >= rows:
            break
        if trees[row][col]:
            count += 1
    return count


all_ = [
    check(1, 1),
    check(3, 1),
    check(5, 1),
    check(7, 1),
    check(1, 2)]

print(all_)
print(math.prod(all_))
