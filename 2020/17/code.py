import sys
from itertools import product

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    pocket_start = [[c for c in l.strip()] for l in f.readlines()]

cycles = 6
x_size_start = len(pocket_start[0])
y_size_start = len(pocket_start)
z_size_start = 1
w_size_start = 1

x_size = x_size_start + 2 * cycles
y_size = y_size_start + 2 * cycles
z_size = z_size_start + 2 * cycles
w_size = w_size_start + 2 * cycles

# pocket3[z][y][x] for better reprensentation
pocket3 = [[['.' for x in range(x_size)] for y in range(y_size)] for z in range(z_size)]
for y in range(len(pocket_start)):
    for x in range(len(pocket_start[0])):
        pocket3[cycles][cycles + y][cycles + x] = pocket_start[y][x]

# pocket4[w][z][y][x] for better reprensentation
pocket4 = [[[['.' for x in range(x_size)] for y in range(y_size)] for z in range(z_size)] for w in range(w_size)]
for y in range(len(pocket_start)):
    for x in range(len(pocket_start[0])):
        pocket4[cycles][cycles][cycles + y][cycles + x] = pocket_start[y][x]


def print_pocket3():
    for z in pocket3:
        for yx in z:
            print(''.join(yx))
        print('=====')


def print_pocket4():
    for w in pocket4:
        for z in w:
            for yx in z:
                print(''.join(yx))
            print('=====')
        print('==========')


def neighbours(p, dim, w, z, y, x):
    out = []
    if dim == 3:
        for dz, dy, dx in product([-1, 0, 1], repeat=3):
            if all(delta == 0 for delta in [dz, dy, dx]):
                continue
            if all(0 <= pos + delta < size for pos, delta, size in zip([z, y, x],
                                                                       [dz, dy, dx],
                                                                       [z_size, y_size, x_size])):
                out += [p[z + dz][y + dy][x + dx]]
    elif dim == 4:
        for dw, dz, dy, dx in product([-1, 0, 1], repeat=4):
            if all(delta == 0 for delta in [dw, dz, dy, dx]):
                continue
            if all(0 <= pos + delta < size for pos, delta, size in zip([w, z, y, x],
                                                                       [dw, dz, dy, dx],
                                                                       [w_size, z_size, y_size, x_size])):
                out += [p[w + dw][z + dz][y + dy][x + dx]]
    return out


def update(p, dim, w, z, y, x):
    if dim == 3:
        state = p[z][y][x]
    elif dim == 4:
        state = p[w][z][y][x]
    n_active = neighbours(p, dim, w, z, y, x).count('#')
    if state == '#' and not (n_active == 2 or n_active == 3):
        return '.'
    elif state == '.' and n_active == 3:
        return '#'
    else:
        return state


def step(p, dim):
    if dim == 3:
        return [[[update(p, dim, 0, z, y, x)
                  for x in range(x_size)]
                 for y in range(y_size)]
                for z in range(z_size)]
    elif dim == 4:
        return [[[[update(p, dim, w, z, y, x)
                   for x in range(x_size)]
                  for y in range(y_size)]
                 for z in range(z_size)]
                for w in range(w_size)]


for i in range(6):
    print(f"Step: {i + 1}")
    pocket3 = step(pocket3, 3)
    print("Dim 3 DONE")
    pocket4 = step(pocket4, 4)
    print("Dim 4 DONE")


active3 = len([pocket3[z][y][x]
              for x in range(x_size)
              for y in range(y_size)
              for z in range(z_size)
              if pocket3[z][y][x] == '#'])
active4 = len([pocket4[w][z][y][x]
              for x in range(x_size)
              for y in range(y_size)
              for z in range(z_size)
              for w in range(w_size)
              if pocket4[w][z][y][x] == '#'])
print(f"{active3=}")
print(f"{active4=}")
