from itertools import combinations

with open('input.txt') as f:
    nums = [int(l) for l in f]


def test(last_n, goal):
    for a, b in combinations(last_n, 2):
        if a + b == goal:
            return True
    return False


n = 25  # preamble length

for i in range(n, len(nums[n:])):
    if not test(nums[i - n:i], nums[i]):
        invalid = nums[i]
        break

print(invalid)


def find_cont(nums, invalid):
    for length in range(2, len(nums)):
        for i in range(len(nums[:-length])):
            if sum(nums[i:i + length]) == invalid:
                return nums[i:i + length]


cont = find_cont(nums, invalid)
print(min(cont) + max(cont))
