from typing import Dict, List

import sys

infile = sys.argv[1] if len(sys.argv) > 1 else "input"
with open(infile) as f:
    rules_str, my_str, nearby_str = f.read().split('\n\n')

rules_list = rules_str.split('\n')
rules = {}
for rule in rules_list:
    field, ranges_str = rule.split(': ')
    ranges = [(int(r[0]), int(r[1])) for r in map(lambda x: x.split('-'), ranges_str.split(' or '))]
    rules[field] = ranges
my = list(map(int, my_str.split('\n')[1].split(',')))
nearby = list(map(lambda t: list(map(int, t.split(','))), nearby_str.split('\n')[1:-1]))


valid_ticket = [my]
invalid_sum = 0
for t in nearby:
    valid = True
    for value in t:
        if not any(r[0][0] <= value <= r[0][1] or r[1][0] <= value <= r[1][1] for r in rules.values()):
            valid = False
            invalid_sum += value
    if valid:
        valid_ticket.append(t)
print(invalid_sum)

idx_options: Dict[str, List[int]] = {}
for field in rules.keys():
    idx_options[field] = []
    for i in range(len(valid_ticket[0])):
        r = rules[field]
        if all(r[0][0] <= ticket[i] <= r[0][1] or r[1][0] <= ticket[i] <= r[1][1] for ticket in valid_ticket):
            idx_options[field].append(i)

idx_map: Dict[str, int] = {}
while True:
    if not idx_options:
        break
    for field, idxs in idx_options.items():
        if len(idxs) == 1:
            idx_map[field] = idxs[0]
            del idx_options[field]
            for field_, idxs_ in idx_options.items():
                if idxs[0] in idxs_:
                    idxs_.remove(idxs[0])
            break

result = 1
for field, idx in idx_map.items():
    if 'departure' in field:
        result *= my[idx]
print(result)
