from __future__ import annotations

import fileinput
from typing import NamedTuple


class Point(NamedTuple):
    r: int
    c: int

    def __repr__(self):
        return f"({self.r}, {self.c})"

    def scan(self):
        return [Point(self.r + dr, self.c + dc)
                for dr in [-1, 0, 1]
                for dc in [-1, 0, 1]]


def blti(bool_list: list[bool]) -> int:
    # bool list to int
    return int("".join([str(int(b)) for b in bool_list]), 2)


class Img(dict[Point, bool]):
    def __init__(self, alg):
        assert len(alg) == 512
        self.a: list[bool] = alg
        self.f: bool = False

    def __missing__(self, _):
        return self.f

    def new(self) -> Img:
        ret = Img(self.a)
        if (not self.f and self.a[0]) or (self.f and self.a[511]):
            ret.f = not self.f
        return ret

    def step(self) -> Img:
        rmin: int = min(p.r for p in self)
        rmax: int = max(p.r for p in self)
        cmin: int = min(p.c for p in self)
        cmax: int = max(p.c for p in self)

        after = self.new()
        for r in range(rmin-1, rmax+2):
            for c in range(cmin-1, cmax+2):
                pixel = Point(r, c)
                idx = blti([self[p] for p in pixel.scan()])
                after[pixel] = self.a[idx]
        return after

    def count(self) -> int:
        if self.f:
            return -1
        return len([p for p in self.values() if p])


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    alg: list[bool] = [c == '#' for c in lines[0]]
    img = Img(alg)
    for row, line in enumerate(lines[2:]):
        for col, char in enumerate(line):
            img[Point(row, col)] = char == '#'

    return [img]


def part_one(img: Img):
    img = img.step()
    img = img.step()

    return img.count()


def part_two(img: Img):
    for _ in range(50):
        img = img.step()
    return img.count()


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
