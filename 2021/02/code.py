with open('input.txt') as f:
    lines = f.readlines()


def part_one():
    position = 0
    depth = 0
    for line in lines:
        direction, value = line.split(" ")
        value = int(value)
        if direction == "forward":
            position += value
        else:
            depth += value * (1 if direction == "down" else -1)
    return position * depth


def part_two():
    position = 0
    depth = 0
    aim = 0
    for line in lines:
        direction, value = line.split(" ")
        value = int(value)
        if direction == "forward":
            position += value
            depth += value * aim
        else:
            aim += value * (1 if direction == "down" else -1)
    return position * depth


def main():
    print("Part 1:", part_one())
    print("Part 2:", part_two())


if __name__ == "__main__":
    main()
