import fileinput
from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Union
from itertools import combinations


class Number(ABC):
    @staticmethod
    def parse(line: str) -> "Pair":
        def parse_rec(idx=0):
            while True:
                if line[idx] == "[":
                    fst, idx = parse_rec(idx+1)
                elif line[idx] == ",":
                    snd, idx = parse_rec(idx+1)
                elif line[idx] == "]":
                    return Pair(fst, snd), idx
                else:
                    return Digit(int(line[idx])), idx
                idx += 1
        return parse_rec()[0]

    @staticmethod
    def add(fst, snd) -> "Pair":
        num = Pair(fst.clone(), snd.clone())

        # reduce
        while True:
            if num.explode():
                continue
            if not num.split():
                break

        return num

    @abstractmethod
    def clone(self) -> "Number": pass

    @abstractmethod
    def merge_left(self, val: int) -> None: pass

    @abstractmethod
    def merge_right(self, val: int) -> None: pass

    @abstractmethod
    def explode(self, depth=0) -> Union[bool, int, tuple[int, int], None]: pass

    @abstractmethod
    def split(self) -> Union[bool, int]: pass

    @abstractmethod
    def magnitude(self) -> int: pass


@dataclass
class Pair(Number):
    left: Number
    right: Number

    def __repr__(self): return f"[{self.left}, {self.right}]"
    def clone(self): return Pair(self.left.clone(), self.right.clone())
    def merge_left(self, val: int): self.left.merge_left(val)
    def merge_right(self, val: int): self.right.merge_right(val)

    def explode(self, depth=0):
        l_ret = self.left.explode(depth+1)

        if l_ret is True:
            return True
        elif isinstance(l_ret, tuple):
            l_lft, l_rght = l_ret
            if isinstance(l_lft, int) and isinstance(l_rght, int):
                self.left = Digit(0)
            if isinstance(l_rght, int):
                self.right.merge_left(l_rght)
                l_rght = None
            if l_lft is None and l_rght is None:
                return True
            else:
                return l_lft, l_rght

        r_ret = self.right.explode(depth+1)
        if isinstance(r_ret, bool):
            return r_ret
        elif isinstance(r_ret, tuple):
            r_lft, r_rght = r_ret
            if isinstance(r_lft, int) and isinstance(r_rght, int):
                self.right = Digit(0)
            if isinstance(r_lft, int):
                self.left.merge_right(r_lft)
                r_lft = None
            if r_lft is None and r_rght is None:
                return True
            else:
                return r_lft, r_rght
        elif isinstance(l_ret, int) and isinstance(r_ret, int):
            return l_ret, r_ret

    def split(self):
        l_ret = self.left.split()
        if l_ret is True:
            return True
        if l_ret is False:
            r_ret = self.right.split()
            if isinstance(r_ret, bool):
                return r_ret
            else:
                fst = Digit(r_ret//2)
                snd = Digit((r_ret+1)//2)
                self.right = Pair(fst, snd)
                return True
        else:
            fst = Digit(l_ret//2)
            snd = Digit((l_ret+1)//2)
            self.left = Pair(fst, snd)
            return True

    def magnitude(self): return (3 * self.left.magnitude()
                                 + 2 * self.right.magnitude())


@dataclass
class Digit(Number):
    value: int

    def __repr__(self): return str(self.value)
    def clone(self) -> "Digit": return Digit(self.value)
    def merge_left(self, val: int) -> None: self.value += val
    def merge_right(self, val: int) -> None: self.value += val
    def explode(self, depth=0): return self.value if depth >= 5 else False
    def split(self): return self.value if self.value > 9 else False
    def magnitude(self) -> int: return self.value


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        numbers = [Number.parse(line.strip()) for line in f]
    return [numbers]


def part_one(N):
    res = N[0]
    for num in N[1:]:
        res = Number.add(res, num)
    return res.magnitude()


def part_two(N):
    maximum = 0
    for a, b in combinations(N, 2):
        res_fwd = Number.add(a, b).magnitude()
        res_bck = Number.add(b, a).magnitude()
        maximum = max(maximum, res_fwd, res_bck)
    return maximum


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
