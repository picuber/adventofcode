import fileinput
import sys


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    crabs = list(map(int, lines[0].split(",")))
    return [crabs]


def const(start, end): return abs(start - end)
def linear(start, end): return sum(range(1, abs(start-end) + 1))


def calc_fuel(crabs, func):
    min_fuel = sys.maxsize
    for pos in range(min(crabs), max(crabs) + 1):
        new_fuel = sum([func(crab, pos) for crab in crabs])
        if new_fuel < min_fuel:
            min_fuel = new_fuel
        else:
            return min_fuel


def part_one(crabs):
    return calc_fuel(crabs, const)


def part_two(crabs):
    return calc_fuel(crabs, linear)


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
