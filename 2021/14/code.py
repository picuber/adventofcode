import fileinput
from collections import Counter


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        rule = {}
        for line in f:
            line = line.strip()
            if f.isfirstline():
                template = line
            elif line:
                pair, char = line.split(" -> ")
                # rule[pair] = char
                rule[pair] = (pair[0] + char, char + pair[1])

    return [template, rule]


def step(poly, R):
    ctr = Counter()
    for pair, (res1, res2) in R.items():
        ctr[res1] += poly[pair]
        ctr[res2] += poly[pair]
    return ctr


def count(poly, start, end):
    c = Counter()
    c[start] = 1
    c[end] = 1
    for pair, num in poly.items():
        c[pair[0]] += num
        c[pair[1]] += num
    return [cnt//2 for cnt in c.values()]


def run_for(T, R, n):
    poly = Counter(T[i:i+2] for i in range(len(T) - 1))
    for i in range(n):
        poly = step(poly, R)
    c = count(poly, T[1], T[-1])
    return max(c) - min(c)


def part_one(T, R):
    return run_for(T, R, 10)


def part_two(T, R):
    return run_for(T, R, 40)


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
