import fileinput
from typing import NamedTuple
from collections import deque


class Point(NamedTuple):
    r: int      # row
    c: int      # column

    def __repr__(self):
        return f"({self.r}, {self.c})"


class Grid(NamedTuple):
    m: dict[Point, int]     # map
    n_r: int = 10           # number of rows
    n_c: int = 10           # number of cols

    def __str__(self):
        out = f"[{self.n_r}, {self.n_c}]\n"
        for r in range(self.n_r):
            for c in range(self.n_c):
                out += f"{self.m[Point(r, c)]}"
            out += "\n"
        return out

    def adj(self, p: Point) -> set[Point]:
        return set(Point(p.r + dr, p.c + dc)
                   for dr in [-1, 0, 1]
                   for dc in [-1, 0, 1]
                   if (dr, dc) != (0, 0)
                   and 0 <= p.r + dr < self.n_r
                   and 0 <= p.c + dc < self.n_c)

    def step(self) -> int:
        flashes: set[Point] = set()

        def incr(o) -> bool:  # returns if it was a new flash
            self.m[o] += 1
            if self.m[o] > 9:
                ret = o not in flashes
                flashes.add(o)
                return ret
            return False

        for o in self.m.keys():
            incr(o)

        Q: deque[Point] = deque(flashes)
        while Q:
            f = Q.popleft()
            for a in self.adj(f):
                if incr(a):
                    Q.append(a)

        for o in flashes:
            self.m[o] = 0

        return len(flashes)


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        grid = {}
        for r, line in enumerate(f):
            for c, h in enumerate(line.strip()):
                p = Point(r, c)
                grid[p] = int(h)

        G = Grid(grid)
    return [G]


def part_one(G):
    return sum(G.step() for _ in range(100))


def part_two(G):
    count = 100
    while G.step() < G.n_r * G.n_c:
        count += 1
    return count + 1


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
