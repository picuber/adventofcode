import fileinput

# You have to enter the input filename as the program argument
# or pipe it via stdin. It won't do anything on no input
with fileinput.input() as f:
    lines = [line.strip() for line in f]
n = len(lines[0])
values = [int(line, 2) for line in lines]


# int(True) = 1, int(False) = 0
def most(zeros, ones): return int(ones >= zeros)
def least(zeros, ones): return int(zeros > ones)


def countBit(values, pos):
    zeros = 0
    ones = 0
    for val in values:
        if (val >> (n - pos)) & 1 == 0:
            zeros += 1
        else:
            ones += 1
    return zeros, ones


def part_one():
    gamma = 0
    epsilon = 0
    for pos in range(1, n + 1):
        zeros, ones = countBit(values, pos)
        gamma = (gamma << 1) | most(zeros, ones)
        epsilon = (epsilon << 1) | least(zeros, ones)
    return gamma * epsilon


def part_two():
    def oxy_rating():
        vals = values[:]
        for pos in range(1, n + 1):
            if len(vals) <= 1:
                break
            crit = most(*countBit(vals, pos))
            vals = list(filter(lambda val: (val >> (n-pos)) & 1 == crit, vals))
        return vals[0]

    def co2_rating():
        vals = values[:]
        for pos in range(1, n + 1):
            if len(vals) <= 1:
                break
            crit = least(*countBit(vals, pos))
            vals = list(filter(lambda val: (val >> (n-pos)) & 1 == crit, vals))
        return vals[0]

    return oxy_rating() * co2_rating()


# All of this could be more efficient when done in one iteration over the list
def main():
    print("Part 1:", part_one())
    print("Part 2:", part_two())


if __name__ == "__main__":
    main()
