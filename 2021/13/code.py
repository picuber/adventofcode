import fileinput
from typing import NamedTuple
from functools import reduce


class Dot(NamedTuple):
    x: int
    y: int

    def __repr__(self):
        return f"({self.x}, {self.y})"


class Instr(NamedTuple):
    axis: str
    value: int


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input

    dots = set()
    instructions = list()
    parse_dots = True
    with fileinput.input() as f:
        for line in f:
            line = line.strip()
            if not line:
                parse_dots = False
                continue
            if parse_dots:
                x, y = line.split(",")
                dots.add(Dot(int(x), int(y)))
            else:
                axis, value = line[11:].split("=")
                instructions.append(Instr(axis, int(value)))

    return [dots, instructions]


def fold(D: set[Dot], i: Instr) -> set[Dot]:
    return (
        set(d if d.x < i.value else Dot(2 * i.value - d.x, d.y) for d in D)
        if i.axis == "x" else
        set(d if d.y < i.value else Dot(d.x, 2 * i.value - d.y) for d in D)
        )


def part_one(D, F):
    return len(fold(D, F[0]))


def part_two(D, Instrs):
    folded = reduce(fold, Instrs, D)
    max_x = max(d[0] for d in folded) + 1
    max_y = max(d[1] for d in folded) + 1
    out = ""
    for y in range(max_y):
        for x in range(max_x):
            out += "#" if Dot(x, y) in folded else "."
        out += "\n"
    return out


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
