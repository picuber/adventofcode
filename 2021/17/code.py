import fileinput
from typing import NamedTuple


class Point(NamedTuple):
    x: int
    y: int

    def __repr__(self):
        return f"P({self.x}, {self.y})"


class Velocity(NamedTuple):
    x: int
    y: int

    def __repr__(self):
        return f"V({self.x}, {self.y})"


class Target(NamedTuple):
    xL: int
    xR: int
    yB: int
    yT: int
    start: Point = Point(0, 0)

    def won(self, p: Point) -> bool:
        return self.xL <= p.x <= self.xR and self.yB <= p.y <= self.yT

    def hits(self, V: Velocity) -> tuple[Point, Point, int]:
        pos = self.start
        vel = V
        max_y = 0
        while pos.x <= self.xR and pos.y >= self.yB:
            prev = pos
            pos = Point(pos.x + vel.x, pos.y + vel.y)
            vel = Velocity(max(0, vel.x - 1), vel.y - 1)
            max_y = max(max_y, pos.y)
        return prev, pos, max_y


def above(T):
    V = Velocity(T.xR, 0)
    max_y = 0
    hit_count = 0
    last_hit = None
    while True:
        prev, pos, peak = T.hits(V)
        if T.won(prev):
            max_y = peak
            hit_count += 1
            last_hit = V
        if last_hit and V.y >= 1.5 * (last_hit.y+1):
            break
        if V.x == 1:
            V = Velocity(T.xR, V.y+1)
        else:
            V = Velocity(V.x-1, V.y)
    return max_y, hit_count


def below(T):
    V = Velocity(T.xR, -1)
    hit_count = 0
    last_hit = None
    while True:
        prev, pos, _ = T.hits(V)
        if T.won(prev):
            hit_count += 1
            last_hit = V
        if last_hit and V.y <= 1.5 * (last_hit.y-1):
            break
        if V.x == 1:
            V = Velocity(T.xR, V.y-1)
        else:
            V = Velocity(V.x-1, V.y)
    return hit_count


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        for line in f:
            x, y = line.split(": ")[1].split(", ")
            xL, xR = x.split("=")[1].split("..")
            yB, yT = y.split("=")[1].split("..")
            target = Target(int(xL), int(xR), int(yB), int(yT))
            break

    max_y, hit_count = above(target)
    hit_count += below(target)
    return [max_y, hit_count]


def part_one(max_y, _):
    return max_y


def part_two(_, hit_count):
    return hit_count


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
