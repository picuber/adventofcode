import fileinput
from typing import NamedTuple


class Sample(NamedTuple):
    signal: list[frozenset[str]]
    output: list[frozenset[str]]


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    def parse_line(line: str) -> Sample:
        signal, output = line.split("|")
        return Sample(
            sorted([frozenset(sig) for sig in signal.split()], key=len),
            [frozenset(out) for out in output.split()])

    samples = [parse_line(line) for line in lines]
    return [samples]


def part_one(samples):
    # lengths: (1: 2), (4: 4), (7, 3), (8: 7)
    unique = [2, 4, 3, 7]
    return sum([
        len([d for d in sample.output if len(d) in unique])
        for sample in samples
    ])


def part_two(samples):
    # unique: 1, 4, 7, 8
    # group 690, len = 6
    #   6: not includes 7
    #   9: includes 4
    #   0:
    # group 235, len = 5
    #   3: includes 1
    #   5: included in 6
    #   2:

    values = []
    for sample in samples:
        signal = sample.signal
        digit = {signal[0]: 1,
                 signal[1]: 7,
                 signal[2]: 4,
                 signal[9]: 8}

        for sig in signal[6:9]:
            if signal[2] <= sig:        # 4 \subset 9
                digit[sig] = 9
            elif not signal[1] <= sig:  # 7 \subset 6
                digit[sig] = 6
                six = sig
            else:
                digit[sig] = 0
        for sig in signal[3:6]:
            if signal[0] <= sig:        # 1 \subset 3
                digit[sig] = 3
            elif sig <= six:
                digit[sig] = 5
            else:
                digit[sig] = 2

        values += [int("".join([str(digit[p]) for p in sample.output]))]
    return sum(values)


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
