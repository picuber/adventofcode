import fileinput
from typing import NamedTuple
from queue import Queue as Q


class Point(NamedTuple):
    r: int      # row
    c: int      # col

    def __repr__(self):
        return f"({self.r}, {self.c})"


class Val(NamedTuple):
    p: Point    # point
    h: int      # value

    def __repr__(self):
        return f"{self.p} = {self.h}"


class Height(NamedTuple):
    m: dict[Point, Val]     # map
    n_r: int                # number of rows
    n_c: int                # number of cols

    def __str__(self):
        out = f"[{self.n_r}, {self.n_c}]\n"
        for r in range(self.n_r):
            for c in range(self.n_c):
                out += f"{self.m[Point(r, c)].h}"
            out += "\n"
        return out


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        height = {}
        for line in f:
            r = f.lineno() - 1
            if f.isfirstline():
                n_col = len(line.strip())
            for c, h in enumerate(line.strip()):
                p = Point(r, c)
                height[p] = Val(p, int(h))

        H = Height(height, f.lineno(), n_col)
    lows = [v for v in H.m.values()
            if all([v.h < n.h for n in adjacent(H, v.p)])]
    return [H, lows]


def adjacent(H, p: Point) -> list[Val]:
    adj = [Point(p.r + dr, p.c + dc)
           for dr in [-1, 0, 1]
           for dc in [-1, 0, 1]
           if abs(dr + dc) == 1]
    return [H.m[p] for p in adj if p in H.m]


def get_basins(H, lows) -> list[int]:
    def bfs(low):
        todo: Q[Val] = Q()
        todo.put(low)
        basin = set()
        while not todo.empty():
            v = todo.get()
            basin.add(v)
            for adj in adjacent(H, v.p):
                if adj not in basin and adj.h != 9:
                    todo.put(adj)
        return len(basin)
    return [bfs(low) for low in lows]


def part_one(H, lows) -> int:
    return sum(map(lambda val: val.h + 1, lows))


def part_two(H, lows) -> int:
    basins = sorted(get_basins(H, lows), reverse=True)
    return basins[0] * basins[1] * basins[2]


def main():
    input_data = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
