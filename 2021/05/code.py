import fileinput
from typing import NamedTuple


class Point(NamedTuple):
    x: int
    y: int

    def __str__(self):
        return f"({self.x}, {self.y})"


class Pipe(NamedTuple):
    start: Point
    end: Point


class Field(dict):
    def __missing__(self, key):
        return 0


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    pipes = [Pipe(*[
        Point(*map(int, point.split(",")))
        for point in line.split(" -> ")
    ]) for line in lines]

    return [pipes]


def step(begin: int, end: int) -> list[int]:
    if begin < end:
        return list(range(begin, end+1))
    elif begin > end:
        return list(range(begin, end-1, -1))
    else:
        return list()


def add_vertical(field, start, end):
    if start.x == end.x:
        for i in step(start.y, end.y):
            field[Point(start.x, i)] += 1


def add_horizontal(field, start, end):
    if start.y == end.y:
        for i in step(start.x, end.x):
            field[Point(i, start.y)] += 1


def add_diagonal(field, start, end):
    if abs(start.x - end.x) == abs(start.y - end.y):
        for i, j in zip(step(start.x, end.x), step(start.y, end.y)):
            field[Point(i, j)] += 1


def part_one(pipes):
    field: Field[Point] = Field()
    for start, end in pipes:
        add_vertical(field, start, end)
        add_horizontal(field, start, end)
    return len([val for val in field.values() if val >= 2])


def part_two(pipes):
    field: Field[Point] = Field()
    for start, end in pipes:
        add_vertical(field, start, end)
        add_horizontal(field, start, end)
        add_diagonal(field, start, end)
    return len([val for val in field.values() if val >= 2])


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
