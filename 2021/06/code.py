import fileinput


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    fishes = list(map(int, lines[0].split(",")))
    return [fishes]


def simulate(fishes, n_days):
    fish_count = [0] * 9
    for f in fishes:
        fish_count[f] += 1

    for _ in range(n_days):
        new = fish_count.pop(0)
        fish_count[6] += new
        fish_count.append(new)
    return sum(fish_count)


def part_one(fishes):
    return simulate(fishes, 80)


def part_two(fishes):
    return simulate(fishes, 256)


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
