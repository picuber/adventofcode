import fileinput
from collections import deque
from typing import NamedTuple


class Node():
    nID: str
    neighbours: set["Node"]

    def __init__(self, nID):
        self.nID = nID
        self.neighbours = set()

    def __repr__(self):
        return f"{self.nID}: {[str(n.nID) for n in self.neighbours]}"

    def __str__(self):
        return self.nID


class Graph(NamedTuple):
    nodes: dict[str, Node]
    start: Node
    end: Node


Path = str


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        nodes: dict[str, Node] = {}
        for line in f:
            fst, snd = line.strip().split("-")
            for n in [fst, snd]:
                if n not in nodes:
                    nodes[n] = Node(n)
            nodes[fst].neighbours.add(nodes[snd])
            nodes[snd].neighbours.add(nodes[fst])

    return [Graph(nodes, nodes["start"], nodes["end"])]


def part_one(G):
    trail: deque[Node] = deque()
    paths = set()

    def dfs(node: Node) -> None:
        if node in trail and (node == G.start or node.nID.islower()):
            return

        trail.append(node)
        if node == G.end:
            paths.add(",".join(map(str, list(trail))))
        else:
            for adj in node.neighbours:
                dfs(adj)

        trail.pop()

    dfs(G.start)
    return len(paths)


def part_two(G):
    trail: deque[Node] = deque()
    paths = set()

    def dfs(node: Node, doubled: bool) -> None:
        if node in trail:
            if node == G.start:
                return
            elif node.nID.islower():
                if doubled:
                    return
                else:
                    doubled = True

        trail.append(node)
        if node == G.end:
            paths.add(",".join(map(str, list(trail))))
        else:
            for adj in node.neighbours:
                dfs(adj, doubled)

        trail.pop()

    dfs(G.start, False)
    return len(paths)


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
