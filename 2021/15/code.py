import fileinput
import heapq
from typing import NamedTuple

Risk = int


class Point(NamedTuple):
    x: int      # col
    y: int      # row

    def __repr__(self):
        return f"({self.x}, {self.y})"


class Field:
    def __init__(self, risk):
        self.risk: list[list[Risk]] = risk  # risk[row][col] = risk[p.y][p.x]
        self.wdth: int = len(risk[0])       # number of cols
        self.hght: int = len(risk)          # number of rows
        self.start = Point(0, 0)
        self.goal = Point(self.wdth - 1, self.hght - 1)

    def adj(self, p: Point) -> set[Point]:
        return set(Point(p.x + dx, p.y + dy)
                   for dx in [-1, 0, 1]
                   for dy in [-1, 0, 1]
                   if abs(dx-dy) == 1
                   and 0 <= p.x + dx < self.wdth
                   and 0 <= p.y + dy < self.hght)

    def at(self, p: Point) -> Risk: return self.risk[p.y][p.x]
    def h(self, p: Point): return (self.wdth - 1 - p.x) + (self.hght - 1 - p.y)


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        risk = [[int(r) for r in line.strip()] for line in f]

    return [Field(risk)]


def a_star(F):
    g: dict[Point, int] = {F.start: 0}
    def f(p: Point): return g[p] + F.h(p)

    class HQ:
        def __init__(self, init):
            self.hq = [(f(init), init)]

        def push(self, elem): return heapq.heappush(self.hq, (f(elem), elem))
        def pop(self): return heapq.heappop(self.hq)[1]

    open_set = HQ(F.start)
    while open_set.hq:
        curr = open_set.pop()
        if curr == F.goal:
            return g[curr]

        for adj in F.adj(curr):
            new_cost = g[curr] + F.at(adj)
            if new_cost < g.get(adj, new_cost+1):
                g[adj] = new_cost
                open_set.push(adj)


def expand(F: Field) -> Field:
    def wrap(x): return x if x < 10 else x - 9
    exp_side = [[wrap(r + i//F.wdth) for i, r in enumerate(row * 5)]
                for row in F.risk]
    exp_down = [[wrap(r + i//F.hght) for r in row]
                for i, row in enumerate(exp_side * 5)]
    return Field(exp_down)


def part_one(F):
    return a_star(F)


def part_two(F):
    return a_star(expand(F))


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
