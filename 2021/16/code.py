import fileinput
from dataclasses import dataclass
from functools import reduce
from operator import mul


@dataclass
class Packet:
    version: int
    typeID: int

    def __repr__(self):
        return (f"{'L' if self.typeID == 4 else 'O'}" +
                f"=(V{self.version}/T{self.typeID}/{self.content})")


@dataclass
class Value(Packet):
    content: int


@dataclass
class Operator(Packet):
    content: list[Packet]


def parse(TX: str, idx=0):
    version = int(TX[idx:idx+3], 2)
    typeID = int(TX[idx+3:idx+6], 2)
    idx += 6

    if typeID == 4:
        val_str = ""
        while True:
            val_str += TX[idx+1:idx+5]
            idx += 5
            if TX[idx - 5] == "0":
                break
        return Value(version, typeID, int(val_str, 2)), idx
    else:
        if TX[idx] == "0":
            idx += 1
            length = int(TX[idx:idx+15], 2)
            idx += 15
            until = idx + length
            content = []
            while idx < until:
                res, idx = parse(TX, idx)
                content.append(res)
            return Operator(version, typeID, content), idx
        else:
            idx += 1
            cnt = int(TX[idx: idx+11], 2)
            idx += 11
            content = []
            for _ in range(cnt):
                res, idx = parse(TX, idx)
                content.append(res)
            return Operator(version, typeID, content), idx


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    hstbs = {
        '0': "0000", '1': "0001", '2': "0010", '3': "0011",
        '4': "0100", '5': "0101", '6': "0110", '7': "0111",
        '8': "1000", '9': "1001", 'A': "1010", 'B': "1011",
        'C': "1100", 'D': "1101", 'E': "1110", 'F': "1111",
    }
    transmissions = [parse("".join(hstbs[c] for c in hex_str))[0]
                     for hex_str in lines]

    return [transmissions]


def part_one(TXs):
    def ver_sum(p: Packet):
        total = p.version
        if isinstance(p, Operator):
            total += sum(ver_sum(pack) for pack in p.content)
        return total

    return [ver_sum(TX) for TX in TXs]


def part_two(TXs):
    def calc(TX: Packet):
        if isinstance(TX, Value):
            return TX.content
        elif isinstance(TX, Operator):
            operands = [calc(p) for p in TX.content]
            if TX.typeID == 0:
                return sum(operands)
            elif TX.typeID == 1:
                return reduce(mul, operands)
            elif TX.typeID == 2:
                return min(operands)
            elif TX.typeID == 3:
                return max(operands)
            elif TX.typeID == 5:
                return int(operands[0] > operands[1])
            elif TX.typeID == 6:
                return int(operands[0] < operands[1])
            elif TX.typeID == 7:
                return int(operands[0] == operands[1])

    return [calc(TX) for TX in TXs]


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
