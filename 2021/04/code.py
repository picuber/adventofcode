import fileinput
from dataclasses import dataclass


@dataclass
class Field:
    num: int
    crossed: bool


class Bingo:
    n_rows: int = 5
    n_cols: int = 5
    # board[row][col]
    board: list[list[Field]]

    def __init__(self, board: list[str]):
        self.board = (
            list(map(lambda row:
                     list(map(lambda number: Field(int(number), False),
                              row.split())),
                     board)))

    def __str__(self):
        out = "+++++\n"
        for row in self.board:
            for field in row:
                out += f"{field.num: >2d}{'X' if field.crossed else ' '} "
            out += "\n"
        return out + "-----"

    def fields(self):
        for row in self.board:
            for field in row:
                yield field

    def check_bingo(self) -> bool:
        for r in range(self.n_rows):
            if all([f.crossed for f in self.board[r]]):
                return True

        for c in range(self.n_cols):
            if all([self.board[r][c].crossed for r in range(self.n_rows)]):
                return True
        return False

    def unmarked(self) -> list[int]:
        return [field.num for field in self.fields() if not field.crossed]

    def cross(self, num: int) -> bool:
        f = list(filter(lambda field: field.num == num, self.fields()))
        if len(f) == 1:
            f[0].crossed = True
            return True
        return False


# You have to enter the input filename as the program argument
# or pipe it via stdin. It won't do anything on no input
with fileinput.input() as f:
    lines = [line.strip() for line in f]


draws = list(map(int, lines[0].split(",")))
boards = [Bingo(lines[i+1:i+6]) for i in range(1, len(lines), 6)]


def part_one():
    for draw in draws:
        for board in boards:
            board.cross(draw)
            if board.check_bingo():
                return sum(board.unmarked()) * draw


def part_two():
    remaining: set[Bingo] = set(boards)
    won: set[Bingo] = set()
    for draw in draws:
        for board in remaining:
            board.cross(draw)
            if board.check_bingo():
                if len(remaining) == 1:
                    return sum(board.unmarked()) * draw
                won.add(board)
        remaining -= won


def main():
    print("Part 1:", part_one())
    print("Part 2:", part_two())


if __name__ == "__main__":
    main()
