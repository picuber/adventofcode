from __future__ import annotations

import fileinput
from collections import defaultdict
from math import dist as euclid
from typing import NamedTuple


class Point(NamedTuple):
    x: int
    y: int
    z: int

    def __repr__(self):
        return f"({self.x}, {self.y}, {self.z})"

    def __add__(self, other):
        if isinstance(other, Point):
            x, y, z = self
            u, v, w = other
            return Point(x + u, y + v, z + w)
        else:
            return NotImplemented

    def __sub__(self, other):
        if isinstance(other, Point):
            x, y, z = self
            u, v, w = other
            return Point(x - u, y - v, z - w)
        else:
            return NotImplemented

    def rotations(self) -> list[Point]:
        """
        Parity: 2 sign = 1 swap
        Perms:
            Even:   [x, y, z], [y, z, x], [z, x, y]
            Odd:    [x, z, y], [y, x, z], [z, y, x]

        Flips:
            Even:   +++, +--, -+-, --+
            Odd:    ++-, +-+, -++, ---
        """

        x, y, z, = self
        perms_even = [self, Point(y, z, x), Point(z, x, y)]
        perms_odd = [Point(x, z, y), Point(y, x, z), Point(z, y, x)]

        rots = []
        for sx, sy, sz in [(1, 1, 1), (1, -1, -1), (-1, 1, -1), (-1, -1, 1)]:
            for px, py, pz in perms_even:
                rots.append(Point(sx*px, sy*py, sz*pz))
        for sx, sy, sz in [(1, 1, -1), (1, -1, 1), (-1, 1, 1), (-1, -1, -1)]:
            for px, py, pz in perms_odd:
                rots.append(Point(sx*px, sy*py, sz*pz))

        return rots


class Scan:
    p: set[Point]
    d: set[int]

    def __init__(self, p: set[Point]):
        self.p = p
        self.d = {int(euclid(a, b)) for a in self.p for b in self.p if a < b}

    def translate(self, dp: Point) -> Scan:
        return Scan(set(p + dp for p in self.p))

    def rotations(self) -> list[Scan]:
        return [Scan(set(rot))
                for rot
                in zip(*[p.rotations() for p in self.p])]

    def overlap(self, o: Scan) -> tuple[Scan, Point] | None:
        """12C2 = 66 => 12 beacons \\equiv 66 dists"""
        if len(self.d & o.d) < 66:
            return None

        for rot in o.rotations():
            votes: dict[Point, int] = defaultdict(int)
            for rp in rot.p:
                for sp in self.p:
                    votes[sp - rp] += 1
            for dp, val in votes.items():
                if val >= 12:
                    return rot.translate(dp), dp
        return None


def build_beacon_map(R: list[Scan]):
    scanners: set[Point] = {Point(0, 0, 0)}
    beacons: set[Point] = set(R[0].p)
    remain = set(R[1:])
    while remain:
        B = Scan(beacons)
        for elem in list(remain):
            if (res := B.overlap(elem)):
                new_beacons, scanner = res
                beacons |= new_beacons.p
                scanners.add(scanner)
                print("Beacons known: ", len(beacons))
                remain.remove(elem)
                break

    return beacons, scanners


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        report: list[Scan] = []
        points: set[Point] = set()
        for line in f:
            line = line.strip()
            if not line:
                continue
            elif line.startswith("---"):
                if points:
                    report.append(Scan(points))
                points = set()
            else:
                points.add(Point(*map(int, line.split(','))))
        report.append(Scan(points))
    beacons, scanners = build_beacon_map(report)
    return [beacons, scanners]


def part_one(B: list[Point], _):
    return len(B)


def part_two(_, S: list[Point]):
    def manhattan(a, b):
        return sum([abs(n - m) for n, m in zip(a, b)])
    return max(manhattan(a, b) for a in S for b in S if a < b)


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
