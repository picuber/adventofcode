import fileinput
from collections import deque
from typing import Union


def process(line) -> Union[str, list[str]]:
    pair = {"(": ")",
            "[": "]",
            "{": "}",
            "<": ">", }

    d: deque[str] = deque()
    for c in line:
        if c in "([{<":
            d.append(c)
        else:
            if pair[d[-1]] == c:
                d.pop()
            else:
                return c
    return list(reversed(d))


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [process(line.strip()) for line in f]

    return [lines]


def part_one(lines):
    score = {")": 3,
             "]": 57,
             "}": 1197,
             ">": 25137}
    return sum([score[c] for c in lines if isinstance(c, str)])


def part_two(lines):
    score = {"(": "1",
             "[": "2",
             "{": "3",
             "<": "4"}
    scores = [int("".join([score[c] for c in line]), 5)
              for line in lines
              if isinstance(line, list)]
    return sorted(scores)[int(len(scores)/2)]


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
