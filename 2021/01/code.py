with open('input.txt') as f:
    lines = f.readlines()


def part_one():
    values = [int(line) for line in lines]
    diffs = [values[i+1] - values[i] for i in range(len(values)-1)]
    return len(list(filter(lambda x: x > 0, diffs)))


def part_two():
    values = [int(line) for line in lines]
    slides = [sum(values[i:i+3]) for i in range(len(values) - 2)]
    diffs = [slides[i+1] - slides[i] for i in range(len(slides)-1)]
    return len(list(filter(lambda x: x > 0, diffs)))


def main():
    print("Part 1:", part_one())
    print("Part 2:", part_two())


if __name__ == "__main__":
    main()
