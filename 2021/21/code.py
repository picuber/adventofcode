from __future__ import annotations

import fileinput
from typing import NamedTuple


class Player(NamedTuple):
    pos: int
    score: int = 0
    def __repr__(self): return f"(P{self.pos}/S{self.score})"

    def moved(self, amount: int) -> Player:
        pos = (self.pos + amount) % 10
        return Player(pos, self.score + (pos if pos > 0 else 10))


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        p1, p2 = [int(line.strip().split(':')[1]) for line in f]

    return [Player(p1), Player(p2)]


def part_one(plr: Player, opp: Player):
    class Die():
        def __init__(self): self.count = 0
        def roll3(self): return self.roll() + self.roll() + self.roll()

        def roll(self):
            self.count += 1
            return self.count

    die = Die()
    while True:
        plr = plr.moved(die.roll3())
        if plr.score >= 1000:
            return die.count * opp.score
        plr, opp = opp, plr


def part_two(plr: Player, opp: Player):
    """ use recursion and dynamic programming
    Idea from: https://www.youtube.com/watch?v=a6ZdJEntK
    """

    # rolls = {3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}
    d3 = [1, 2, 3]
    rolls = [a+b+c for a in d3 for b in d3 for c in d3]
    DP: dict[tuple[Player, Player], tuple[int, int]] = {}

    def count(plr, opp):
        if plr.score >= 21:
            return 1, 0
        if opp.score >= 21:
            return 0, 1
        if (plr, opp) in DP:
            return DP[plr, opp]
        wins = 0, 0
        for roll in rolls:
            o_win, p_win = count(opp, plr.moved(roll))
            wins = wins[0] + p_win, wins[1] + o_win
        DP[plr, opp] = wins
        return wins

    return max(count(plr, opp))


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
