import fileinput


def setup() -> list:
    # You have to enter the input filename as the program argument
    # or pipe it via stdin. It won't do anything on no input
    with fileinput.input() as f:
        lines = [line.strip() for line in f]

    elves: list[int] = list()
    current: int = 0
    for line in lines:
        if line == "":
            elves.append(current)
            current = 0
            continue
        current += int(line)

    return [elves]


def part_one(elves: list[int]):
    return max(elves)


def part_two(elves: list[int]):
    return sum(sorted(elves, reverse=True)[0:3])


def main():
    input_data: list = setup()
    print("Part 1:", part_one(*input_data))
    print("Part 2:", part_two(*input_data))


if __name__ == "__main__":
    main()
