use itertools::Itertools;
use std::collections::HashSet;

fn split_compartments(rucksack: &str) -> (String, String) {
    let len = rucksack.len() / 2;
    (rucksack[..len].to_string(), rucksack[len..].to_string())
}

fn get_priority(c: &char) -> u32 {
    if c.is_ascii_lowercase() {
        *c as u32 - 'a' as u32 + 1
    } else if c.is_ascii_uppercase() {
        *c as u32 - 'A' as u32 + 27
    } else {
        panic!("Invalid character")
    }
}

type Bag = HashSet<char>;

fn part1(input: &str) -> u32 {
    input
        .lines()
        .map(|line| {
            let (fst, snd) = split_compartments(line);
            let fst_set: Bag = HashSet::from_iter(fst.chars());
            let snd_set: Bag = HashSet::from_iter(snd.chars());
            let mut inters = fst_set.intersection(&snd_set);
            let elem = inters.next().unwrap();
            get_priority(elem)
        })
        .sum()
}

fn part2(input: &str) -> u32 {
    input
        .lines()
        .chunks(3)
        .into_iter()
        .map(|chunk| {
            let bags: Vec<Bag> = chunk.map(|line| HashSet::from_iter(line.chars())).collect();
            let inters1 = bags[0].intersection(&bags[1]).cloned().collect::<Bag>();
            let mut inters2 = inters1.intersection(&bags[2]);
            let elem = inters2.next().unwrap();
            get_priority(elem)
        })
        .sum()
}

fn main() {
    let input: &str = include_str!("input");
    println!("{}", part1(input));
    println!("{}", part2(input));
}

#[cfg(test)]
mod test {
    use crate::*;

    const EX_INPUT: &str = include_str!("example");

    #[test]
    fn example_part1() {
        assert_eq!(part1(EX_INPUT), 157)
    }

    #[test]
    fn example_part2() {
        assert_eq!(part2(EX_INPUT), 70)
    }
}
