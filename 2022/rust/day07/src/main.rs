use std::collections::BTreeMap;

#[derive(Clone)]
enum Node {
    File { name: String, size: u32 },
    Dir { name: String, size: Option<u32> },
}

type Path<'a> = Vec<&'a str>;
type Directory = Vec<Node>;
type Drive<'a> = BTreeMap<Path<'a>, Directory>;

fn parse(input: &str) -> Drive {
    let mut drive: Drive = BTreeMap::new();
    let mut cwd: Path = vec![];
    let mut curr: Option<Directory> = None;
    for line in input.lines() {
        if line == "$ ls" {
            curr = Some(vec![]);
        } else if line == "$ cd /" {
            match &curr {
                Some(directory) => {
                    drive.insert(cwd.clone(), directory.to_vec());
                }
                None => {}
            }
            cwd.clear();
            cwd.push("/");
        } else if line == "$ cd .." {
            cwd.pop();
        } else if line.starts_with("$ cd ") {
            cwd.push(&line[5..]);
        } else if line.starts_with("dir ") {
        } else {
            // let file = Node::dir(curr, &line[4..]);
            // curr.children.as_ref().unwrap().push(file)
        }
    }
    drive
}

fn part1(data: Drive) -> u32 {
    0
}

fn part2(data: Drive) -> u32 {
    0
}

fn main() {
    let input: &str = include_str!("input");
    let data = parse(input);
    println!("{}", part1(data.clone()));
    println!("{}", part2(data));
}

#[cfg(test)]
mod test {
    use crate::*;

    const EX_INPUT: &str = include_str!("example");
    #[test]
    fn example_part1() {
        assert_eq!(part1(parse(EX_INPUT)), 95437);
    }

    #[test]
    fn example_part2() {
        assert_eq!(part2(parse(EX_INPUT)), 0);
    }
}
