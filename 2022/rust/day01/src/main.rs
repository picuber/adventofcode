fn parse(input: &str) -> Vec<i32> {
    input
        .split("\n\n")
        .map(|elf| {
            elf.lines()
                .map(|item| item.parse::<i32>().unwrap())
                .sum::<i32>()
        })
        .collect()
}

fn part1(data: &Vec<i32>) -> i32 {
    data.iter().max().unwrap().to_owned()
}

fn part2(data: &Vec<i32>) -> i32 {
    let mut data = data.clone();
    data.sort_by(|a, b| b.cmp(a));
    data.iter().take(3).sum::<i32>()
}

fn main() {
    let data = parse(include_str!("input"));
    println!("{}", part1(&data));
    println!("{}", part2(&data));
}

#[cfg(test)]
mod test {
    use crate::*;

    const EX_INPUT: &str = include_str!("example");

    #[test]
    fn example_part1() {
        assert_eq!(part1(&parse(EX_INPUT)), 24000)
    }

    #[test]
    fn example_part2() {
        assert_eq!(part2(&parse(EX_INPUT)), 45000)
    }
}
