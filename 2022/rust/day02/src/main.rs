use std::str::FromStr;

#[derive(Clone)]
enum Opponent {
    A,
    B,
    C,
}

impl FromStr for Opponent {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "A" => Ok(Self::A),
            "B" => Ok(Self::B),
            "C" => Ok(Self::C),
            _ => Err(()),
        }
    }
}

#[derive(Clone)]
enum Me {
    X,
    Y,
    Z,
}
impl Me {
    fn play_regular(self: &Self, opp: &Opponent) -> i32 {
        (match (self, opp) {
            (Self::X, Opponent::B) | (Self::Y, Opponent::C) | (Self::Z, Opponent::A) => 0,
            (Self::X, Opponent::A) | (Self::Y, Opponent::B) | (Self::Z, Opponent::C) => 3,
            (Self::X, Opponent::C) | (Self::Y, Opponent::A) | (Self::Z, Opponent::B) => 6,
        }) + (match self {
            Self::X => 1,
            Self::Y => 2,
            Self::Z => 3,
        })
    }

    fn play_adapted(self: &Self, opp: &Opponent) -> i32 {
        (match self {
            Self::X => 0,
            Self::Y => 3,
            Self::Z => 6,
        }) + (match (self, opp) {
            (Self::X, Opponent::B) | (Self::Y, Opponent::A) | (Self::Z, Opponent::C) => 1,
            (Self::X, Opponent::C) | (Self::Y, Opponent::B) | (Self::Z, Opponent::A) => 2,
            (Self::X, Opponent::A) | (Self::Y, Opponent::C) | (Self::Z, Opponent::B) => 3,
        })
    }
}

impl FromStr for Me {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "X" => Ok(Self::X),
            "Y" => Ok(Self::Y),
            "Z" => Ok(Self::Z),
            _ => Err(()),
        }
    }
}

fn parse(input: &str) -> Vec<(Opponent, Me)> {
    input
        .lines()
        .map(|line| {
            (
                Opponent::from_str(&line[..1]).unwrap(),
                Me::from_str(&line[2..]).unwrap(),
            )
        })
        .collect()
}

fn part1(data: Vec<(Opponent, Me)>) -> i32 {
    data.iter().map(|(opp, me)| me.play_regular(opp)).sum()
}

fn part2(data: Vec<(Opponent, Me)>) -> i32 {
    data.iter().map(|(opp, me)| me.play_adapted(opp)).sum()
}

fn main() {
    let data = parse(include_str!("input"));
    println!("{}", part1(data.clone()));
    println!("{}", part2(data));
}

#[cfg(test)]
mod test {
    use crate::*;

    #[test]
    fn example_part1() {
        assert_eq!(part1(parse(include_str!("example"))), 15)
    }

    #[test]
    fn example_part2() {
        assert_eq!(part2(parse(include_str!("example"))), 12)
    }
}
