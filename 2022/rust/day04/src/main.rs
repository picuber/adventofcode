use std::str::FromStr;

#[derive(Clone, Copy, Debug)]
struct Range {
    start: u32,
    end: u32,
}
impl Range {
    fn contains(self: &Self, other: &Self) -> bool {
        self.start <= other.start && self.end >= other.end
    }

    fn overlap(self: &Self, other: &Self) -> bool {
        (self.start >= other.start && self.start <= other.end)
            || (self.end >= other.start && self.end <= other.end)
            || (self.start <= other.start && self.end >= other.end)
    }
}
impl FromStr for Range {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split("-").collect();
        Ok(Range {
            start: parts[0].parse::<u32>().unwrap(),
            end: parts[1].parse::<u32>().unwrap(),
        })
    }
}

type T = (Range, Range);

fn parse(input: &str) -> Vec<T> {
    input
        .lines()
        .map(|line| {
            let ranges: Vec<Range> = line
                .split(",")
                .map(|r| Range::from_str(r).unwrap())
                .collect();
            (ranges[0], ranges[1])
        })
        .collect()
}

fn part1(data: &Vec<T>) -> usize {
    data.iter()
        .filter(|(a, b)| a.contains(b) || b.contains(a))
        .count()
}

fn part2(data: &Vec<T>) -> usize {
    data.iter().filter(|(a, b)| a.overlap(b)).count()
}

fn main() {
    let input: &str = include_str!("input");
    let data = parse(input);
    println!("{}", part1(&data));
    println!("{}", part2(&data));
}

#[cfg(test)]
mod test {
    use crate::*;

    const EX_INPUT: &str = include_str!("example");

    #[test]
    fn example_part1() {
        assert_eq!(part1(&parse(EX_INPUT)), 2)
    }

    #[test]
    fn example_part2() {
        assert_eq!(part2(&parse(EX_INPUT)), 4)
    }
}
