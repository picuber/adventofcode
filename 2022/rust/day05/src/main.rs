use std::str::FromStr;

type Stack = Vec<Crate>;
// type Stacks<const N: usize> = [Stack; N]
#[derive(Clone, Debug)]
struct Stacks<const N: usize> {
    stack: [Stack; N],
}
impl<const N: usize> Stacks<N>
where
    [Stack; N]: Default,
{
    fn new() -> Self {
        Self {
            stack: Default::default(),
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct Crate {
    val: char,
}
impl FromStr for Crate {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "    " {
            return Err(());
        };

        //TODO
        Err(())
    }
}

#[derive(Clone, Debug)]
struct Move {
    amount: usize,
    from: usize,
    to: usize,
}
impl FromStr for Move {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let from_split: Vec<&str> = s[5..].split(" from ").collect();
        let to_split: Vec<&str> = from_split[1].split(" to ").collect();

        Ok(Move {
            amount: from_split[0].parse().unwrap(),
            from: to_split[0].parse::<usize>().unwrap() - 1,
            to: to_split[1].parse::<usize>().unwrap() - 1,
        })
    }
}

#[derive(Clone)]
struct Port<const N: usize> {
    stacks: Stacks<N>,
    moves: Vec<Move>,
}

fn parse<const N: usize>(input: &str) -> Port<N>
where
    [Stack; N]: Default,
{
    let split: Vec<&str> = input.split("\n\n").collect();
    let mut stacks: Stacks<N> = Stacks::new();
    split[0].lines().rev().skip(1).for_each(|line| {
        line.chars()
            .skip(1)
            .step_by(4)
            .enumerate()
            .for_each(|(i, val)| {
                if val != ' ' {
                    stacks.stack[i].push(Crate { val })
                }
            });
    });

    let moves: Vec<Move> = split[1]
        .lines()
        .map(|line| Move::from_str(line).unwrap())
        .collect();

    Port { stacks, moves }
}

fn get_top<const N: usize>(stacks: &Stacks<N>) -> String {
    stacks
        .stack
        .iter()
        .map(|stack| stack.get(stack.len() - 1).unwrap().val)
        .collect()
}

fn part1<const N: usize>(Port { mut stacks, moves }: Port<N>) -> String {
    for m in moves {
        for _ in 0..m.amount {
            let c = stacks.stack[m.from].pop().unwrap();
            stacks.stack[m.to].push(c)
        }
    }
    get_top(&stacks)
}

fn part2<const N: usize>(Port { mut stacks, moves }: Port<N>) -> String {
    for m in moves {
        let from_len = stacks.stack[m.from].len();
        let mut buf: Vec<Crate> = stacks.stack[m.from].drain(from_len - m.amount..).collect();
        stacks.stack[m.to].append(&mut buf);
    }
    get_top(&stacks)
}

fn main() {
    let input: &str = include_str!("input");
    let data = parse::<9>(input);
    println!("{}", part1(data.clone()));
    println!("{}", part2(data));
}

#[cfg(test)]
mod test {
    use crate::*;

    const EX_INPUT: &str = include_str!("example");

    #[test]
    fn example_part1() {
        assert_eq!(part1(parse::<3>(EX_INPUT)), "CMZ")
    }

    #[test]
    fn example_part2() {
        assert_eq!(part2(parse::<3>(EX_INPUT)), "MCD")
    }
}
