use std::{char, collections::{VecDeque, HashSet}};

fn parse(input: &str) -> Vec<char> {
    let mut char_it = input.chars();
    char_it.next_back();
    char_it.collect()
}

fn has_duplicates(buf: &VecDeque<char>) -> bool {
    HashSet::<&char>::from_iter(buf).len() < buf.len()
}

fn do_work(data: Vec<char>, n: i32) -> usize {
    let mut data_it = data.into_iter().enumerate();

    let mut buf: VecDeque<char> = VecDeque::new();
    for _ in 0..(n-1) {
        if let Some((_, c)) = data_it.next() {
            buf.push_back(c);
        }
    }

    for (i, c) in data_it {
        buf.push_back(c);
        if !has_duplicates(&buf) {
            return i + 1
        }
        buf.pop_front();
    }

    return 0
}


fn part1(data: Vec<char>) -> usize {
    do_work(data, 4)
}

fn part2(data: Vec<char>) -> usize {
    do_work(data, 14)
}

fn main() {
    let input: &str = include_str!("input");
    let data = parse(input);
    println!("{}", part1(data.clone()));
    println!("{}", part2(data));
}

#[cfg(test)]
mod test {
    use crate::*;

    const EX_INPUT: &str = include_str!("example");
    const EX_INPUT1: &str = include_str!("example1");
    const EX_INPUT2: &str = include_str!("example2");
    const EX_INPUT3: &str = include_str!("example3");
    const EX_INPUT4: &str = include_str!("example4");

    #[test]
    fn example_part1() {
        assert_eq!(part1(parse(EX_INPUT)), 7);
        assert_eq!(part1(parse(EX_INPUT1)), 5);
        assert_eq!(part1(parse(EX_INPUT2)), 6);
        assert_eq!(part1(parse(EX_INPUT3)), 10);
        assert_eq!(part1(parse(EX_INPUT4)), 11);
    }

    #[test]
    fn example_part2() {
        assert_eq!(part2(parse(EX_INPUT)), 19);
        assert_eq!(part2(parse(EX_INPUT1)), 23);
        assert_eq!(part2(parse(EX_INPUT2)), 23);
        assert_eq!(part2(parse(EX_INPUT3)), 29);
        assert_eq!(part2(parse(EX_INPUT4)), 26);
    }
}
